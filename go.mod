module gitlab.com/milan44/logger-v2

go 1.15

require (
	github.com/gin-gonic/gin v1.7.4
	github.com/gookit/color v1.5.0
)
